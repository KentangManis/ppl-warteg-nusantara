<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class BahanSeeder extends Seeder
{
	public function run()
	{
        $datas = [
            [
                'nama' => 'Beras',
                'stok' => '1',
                'harga' => '11000'
            ],
            [
                'nama' => 'Bawang',
                'stok' => '1',
                'harga' => '15000'
            ],
            [
                'nama' => 'Garam',
                'stok' => '1',
                'harga' => '12000'
            ],
        ];

        // Using Query Builder
        $this->db->table('bahans')->insertBatch($datas);
	}
}
