<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run()
    {
        $datas = [
            [
                'nama' => 'Admin Warteg',
                'username' => 'admin',
                'password' => password_hash('admin', PASSWORD_BCRYPT),
                'jabatan' => 'admin'
            ],
            [
                'nama' => 'Kasir Warteg',
                'username' => 'kasir',
                'password' => password_hash('kasir', PASSWORD_BCRYPT),
                'jabatan' => 'kasir'
            ],
            [
                'nama' => 'Koki Warteg',
                'username' => 'koki',
                'password' => password_hash('koki', PASSWORD_BCRYPT),
                'jabatan' => 'koki'
            ],
            [
                'nama' => 'Owner Warteg',
                'username' => 'owner',
                'password' => password_hash('owner', PASSWORD_BCRYPT),
                'jabatan' => 'owner'
            ],
        ];

        // Using Query Builder
        $this->db->table('users')->insertBatch($datas);
    }
}
