<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class ProdukSeeder extends Seeder
{
	public function run()
	{
        $datas = [
            [
                'nama' => 'Telur Balado',
                'stok' => '1',
                'harga' => '5000'
            ],
            [
                'nama' => 'Ayam Bakar Madu',
                'stok' => '1',
                'harga' => '15000'
            ],
            [
                'nama' => 'Sayur Lodeh',
                'stok' => '1',
                'harga' => '5000'
            ],
            [
                'nama' => 'Nasi Putih',
                'stok' => '1',
                'harga' => '2000'
            ],
        ];

        // Using Query Builder
        $this->db->table('produks')->insertBatch($datas);
	}
}
