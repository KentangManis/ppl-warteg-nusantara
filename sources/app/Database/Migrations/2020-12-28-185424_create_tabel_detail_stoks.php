<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateTabelDetailStoks extends Migration
{
	public function up()
	{
        // Membuat kolom/field
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
                'auto_increment' => true
            ],
            'stok_id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
            ],
            'bahan_id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
            ],
            'tanggal_kadaluarsa' => [
                'type' => 'DATE',
                'null' => false,
                // 'default' => 'NOW()',
            ],
            'jumlah' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'null' => false
            ],
            'satuan' => [
                'type' => 'VARCHAR',
                'constraint' => '25',
                'null' => false,
            ],
            'harga_satuan' => [
                'type' => 'DECIMAL',
                'constraint' => '18,2',
                'null' => false
            ],
            'harga_total' => [
                'type' => 'DECIMAL',
                'constraint' => '18,2',
                'null' => false
            ],
        ]);

        // Membuat primary key
        $this->forge->addKey('id', TRUE);

        // Membuat foreign key
        $this->forge->addForeignKey('bahan_id','bahans','id');
        $this->forge->addForeignKey('stok_id','stoks','id');

        // Membuat tabel
        $this->forge->createTable('detail_stoks', TRUE);
	}

	//--------------------------------------------------------------------

	public function down()
	{
        $this->forge->dropTable('detail_stoks');
	}
}
