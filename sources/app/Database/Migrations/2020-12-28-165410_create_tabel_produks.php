<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateTabelProduks extends Migration
{
	public function up()
	{
        // Membuat kolom/field
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true
            ],
            'nama' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => false,
            ],
            'jenis' => [
                'type' => 'ENUM',
                'constraint' => ['makanan', 'minuman'],
                // 'default' => 'guest',
            ],
            'stok' => [
                'type' => 'INT',
                'constraint' => 5,
                'null' => false,
                'default' => 0,
            ],
            'harga' => [
                'type' => 'DECIMAL',
                'constraint' => '18,2',
                'default' => 0,
            ],
        ]);

        // Membuat primary key
        $this->forge->addKey('id', TRUE);

        // Membuat tabel
        $this->forge->createTable('produks', TRUE);
	}

	//--------------------------------------------------------------------

	public function down()
	{
        $this->forge->dropTable('produks');
	}
}
