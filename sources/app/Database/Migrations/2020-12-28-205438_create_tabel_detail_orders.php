<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateTabelDetailOrders extends Migration
{
	public function up()
	{
        // Membuat kolom/field
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
                'auto_increment' => true
            ],
            'order_id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
            ],
            'produk_id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
            ],
            'jumlah' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'null' => false
            ],
            'harga_satuan' => [
                'type' => 'DECIMAL',
                'constraint' => '18,2',
                'null' => false
            ],
            'harga_total' => [
                'type' => 'DECIMAL',
                'constraint' => '18,2',
                'null' => false
            ],
        ]);

        // Membuat primary key
        $this->forge->addKey('id', TRUE);

        // Membuat foreign key
        $this->forge->addForeignKey('produk_id','produks','id');
        $this->forge->addForeignKey('order_id','orders','id');

        // Membuat tabel
        $this->forge->createTable('detail_orders', TRUE);
	}

	//--------------------------------------------------------------------

	public function down()
	{
        $this->forge->dropTable('detail_orders');
	}
}
