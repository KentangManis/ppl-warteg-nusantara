<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateTabelOrders extends Migration
{
	public function up()
	{
        // Membuat kolom/field
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
                'auto_increment' => true
            ],
            'user_id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
            ],
            'tanggal' => [
                'type' => 'DATE',
                'null' => false,
                // 'default' => 'NOW()',
            ],
            'total_harga' => [
                'type' => 'DECIMAL',
                'constraint' => '18,2',
                'null' => false
            ],
            'jumlah' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'null' => false
            ],
            'nominal_bayar' => [
                'type' => 'DECIMAL',
                'constraint' => '18,2',
                'null' => false
            ],
            'nominal_kembalian' => [
                'type' => 'DECIMAL',
                'constraint' => '18,2',
                'null' => false
            ],
        ]);

        // Membuat primary key
        $this->forge->addKey('id', TRUE);

        // Membuat foreign key
        $this->forge->addForeignKey('user_id','users','id');

        // Membuat tabel
        $this->forge->createTable('orders', TRUE);
	}

	//--------------------------------------------------------------------

	public function down()
	{
        $this->forge->dropTable('orders');
	}
}
