<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateTabelUsers extends Migration
{
	public function up()
	{
        // Membuat kolom/field
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true
            ],
            'nama' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => false,
            ],
            'username' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => false,
            ],
            'password' => [
                'type' => 'TEXT',
                'null' => false,
            ],
            'jabatan' => [
                'type' => 'ENUM',
                'constraint' => ['kasir', 'koki', 'owner', 'admin', 'guest'],
                // 'default' => 'guest',
            ],
        ]);

        // Membuat primary key
        $this->forge->addKey('id', TRUE);

        // Membuat tabel
        $this->forge->createTable('users', TRUE);
	}

	//--------------------------------------------------------------------

	public function down()
	{
        $this->forge->dropTable('users');
	}
}
