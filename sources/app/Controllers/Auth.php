<?php namespace App\Controllers;

use App\Models\UserModel;

class Auth extends BaseController
{
    public function index()
    {
        if(!empty($this->session->get('loggedUser'))) {
            return redirect()->to('/home');
        }

        $errors = $this->session->getFlashdata('errors');
        return view('login');
    }

    public function login()
    {
        // Ngambil Object
        $post = $this->request->getPost();
        $userModel = new UserModel;

        // Validasi Form
        $validation = $this->validation;
        $validation->setRules($userModel->authRules());

        // Form Validation Gagal
        if($validation->withRequest($this->request)->run() == FALSE){
            return $this->failValidation($validation->getErrors());
        }

        // Proses Data
        $user = $userModel->where('username',$post['username'])->first();
        if(!$user) {
            return $this->failValidation('User Tidak Ditemukan!');
        }

        if(password_verify($post['password'],$user['password'])) {
            $this->session->set('loggedUser', [
                'user_id'=>$user['id'],
                'nama'=>$user['nama'],
                'username'=>$user['username'],
                'jabatan'=>$user['jabatan'],
            ]);

            return redirect()->to('/home');
        }else{
            return $this->failValidation('User / Password Tidak Sesuai!');
        }
    }

    public function logout()
    {
        $this->session->destroy();
        return redirect()->to('/');
    }

}
