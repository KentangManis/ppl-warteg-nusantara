<?php namespace App\Controllers;

use App\Models\UserModel;

class User extends BaseController
{
    protected $redirect_to = '/user';
    protected $module_name = 'Data User';
    protected $pagination = 10;

    public function index()
    {
        // Data
        $data['title'] = "Manajemen User Aplikasi";

        // Model
        $userModel = new UserModel;
        $data['dataObj'] = $userModel->paginate($pagination);
        $data['pager'] = $userModel->pager;

        return $this->renderTemplate('user/home',$data);
    }

    public function create()
    {
        // Ngambil Object
        $post = $this->request->getPost();
        $userModel = new UserModel;

        // Validasi Form
        $validation = $this->validation;
        $validation->setRules($userModel->formRules());

        // Form Validation Gagal
        if($validation->withRequest($this->request)->run() == FALSE){
            return $this->failValidation($validation->getErrors());
        }

        // Proses Data
        $data = [
            'nama' => $post['nama'],
            'username' => $post['username'],
            'password' => $post['password'],
            'jabatan' => $post['jabatan'],
        ];
        $userModel->insert($data);
        
        $this->saveSuccess($this->$module_name);
        return redirect()->to($this->redirect_to);
    }

    public function edit()
    {
        // Ngambil Object
        $post = $this->request->getPost();
        $userModel = new UserModel;

        // Validasi Form
        $validation = $this->validation;
        $validation->setRules($userModel->formRules());

        // Form Validation Gagal
        if($validation->withRequest($this->request)->run() == FALSE){
            return $this->failValidation($validation->getErrors());
        }

        // Proses Data
        $data = [
            'nama' => $post['nama'],
            'username' => $post['username'],
            'password' => $post['password'],
            'jabatan' => $post['jabatan'],
        ];
        $userModel->update($this->request->getPost('id'),$data);

        $this->saveSuccess($this->$module_name);
        return redirect()->to($this->redirect_to);
    }

    public function delete()
    {
        $userModel = new UserModel;
        $userModel->delete($this->request->getPost('id'));
        $this->delSuccess($this->$module_name);
        return redirect()->to($this->redirect_to);

    }
}
