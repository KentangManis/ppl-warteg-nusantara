<?php namespace App\Controllers;

use App\Models\OrderModel;
use App\Models\ProdukModel;
use App\Models\DetailOrderModel;

class Order extends BaseController
{
    protected $redirect_to = '/order';
    protected $module_name = 'Data Inventory';
    protected $jenis_produk = ['makanan', 'minuman'];
    protected $pg = 10;

    public function index()
    {
        // Data
        $data['title'] = "POS Restoran";

        // Model
        $orderModel = new OrderModel;
        $data['dataObj'] = $orderModel->findAll();
        // $data['pager'] = $orderModel->pager;

        return $this->renderTemplate('order/home',$data);
    }

    public function create()
    {
        // Data
        $data['title'] = "Form - POS Restoran";

        $produkModel = new ProdukModel;
        $data['produks'] = $produkModel->findAll();
        $data['jenis_produk'] = $this->jenis_produk;

        return $this->renderTemplate('order/form',$data);
    }

    public function edit($id=false)
    {
        // Data
        $data['title'] = "Form - POS Restoran";

        // Model
        $orderModel = new OrderModel;
        $produkModel = new ProdukModel;
        $detailOrderModel = new DetailOrderModel;
        $data['dataSet'] = $orderModel->find($id);
        if(empty($data['dataSet'])) {
            $this->notFound($this->$module_name);
            return redirect()->to($this->redirect_to);
        }else{
            $data['details'] = $detailOrderModel->where('order_id',$id)->findAll();
        }

        $data['produks'] = $produkModel->findAll();
        $data['jenis_produk'] = $this->jenis_produk;

        return $this->renderTemplate('order/form',$data);
    }

    public function store()
    {
        // Ngambil Object
        $post = $this->request->getPost();
        $orderModel = new OrderModel;

        // Validasi Form
        $validation = $this->validation;
        if($post['id']) {
            $validation->setRules($orderModel->formRules('edit'));
        }else{
            $validation->setRules($orderModel->formRules());
        }

        // Form Validation Gagal
        if($validation->withRequest($this->request)->run() == FALSE){
            return $this->failValidation($validation->getErrors());
        }

        // Proses Data
        $data = [
            'tanggal' => $post['tanggal'],
            'total_harga' => $post['total_harga'],
            'jumlah' => array_sum($post['jumlah']),
            'nominal_bayar' => $post['nominal_bayar'],
            'nominal_kembalian' => $post['nominal_kembalian'],
            'user_id' => $this->session->get('loggedUser')['user_id'],
        ];
        if($post['id']) {
            $orderModel->update($post['id'],$data);
            $order_id = $post['id'];
        }else{
            $orderModel->insert($data);
            $order_id = $orderModel->getInsertID();
        }

        // Resep
        $detailOrderModel = new DetailOrderModel;
        $detailOrderModel->where('order_id',$order_id)->delete();
        $produk = [];
        foreach($post['produk'] as $k=>$v) {
            $produk[] = [
                'produk_id' => $v,
                'order_id' => $order_id,
                'jumlah' => $post['jumlah'][$k],
                'harga_satuan' => $post['harga_satuan'][$k],
                'harga_total' => $post['harga_total'][$k],
            ];
        }
        $detailOrderModel->insertBatch($produk);
        
        $this->saveSuccess($this->$module_name);
        return redirect()->to($this->redirect_to);
    }

    public function delete($id=false)
    {
        // Model
        $orderModel = new OrderModel;
        $detailOrderModel = new DetailOrderModel;
        $data = $orderModel->find($id);

        if(empty($data)) {
            $this->notFound($this->$module_name);
        }else{
            $detailOrderModel->where('order_id',$id)->delete();
            $orderModel->delete($id);
            $this->delSuccess($this->$module_name);
        }

        return redirect()->to($this->redirect_to);
    }
}
