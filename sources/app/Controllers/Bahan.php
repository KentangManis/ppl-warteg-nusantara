<?php namespace App\Controllers;

use App\Models\BahanModel;

class Bahan extends BaseController
{
    protected $redirect_to = '/bahan';
    protected $module_name = 'Data Bahan';
    protected $pg = 10;

    public function index()
    {
        // Data
        $data['title'] = "Manajemen Bahan";

        // Model
        $bahanModel = new BahanModel;
        $data['dataObj'] = $bahanModel->findAll();
        // $data['pager'] = $bahanModel->pager;

        return $this->renderTemplate('bahan/home',$data);
    }

    public function create()
    {
        // Data
        $data['title'] = "Form - Manajemen Bahan";
        $data['jenis_produk'] = $this->jenis_produk;

        return $this->renderTemplate('bahan/form',$data);
    }

    public function edit($id=false)
    {
        // Data
        $data['title'] = "Form - Manajemen Bahan";

        // Model
        $bahanModel = new BahanModel;
        $data['dataSet'] = $bahanModel->find($id);
        if(empty($data['dataSet'])) {
            $this->notFound($this->$module_name);
            return redirect()->to($this->redirect_to);
        }
        $data['jenis_produk'] = $this->jenis_produk;

        return $this->renderTemplate('bahan/form',$data);
    }

    public function store()
    {
        // Ngambil Object
        $post = $this->request->getPost();
        $bahanModel = new BahanModel;

        // Validasi Form
        $validation = $this->validation;
        if($post['id']) {
            $validation->setRules($bahanModel->formRules('edit'));
        }else{
            $validation->setRules($bahanModel->formRules());
        }

        // Form Validation Gagal
        if($validation->withRequest($this->request)->run() == FALSE){
            return $this->failValidation($validation->getErrors());
        }

        // Proses Data
        $data = [
            'nama' => $post['nama'],
            'stok' => $post['stok'],
            'harga' => $post['harga'],
        ];
        if($post['id']) {
            $bahanModel->update($this->request->getPost('id'),$data);
        }else{
            $bahanModel->insert($data);
        }
        
        $this->saveSuccess($this->$module_name);
        return redirect()->to($this->redirect_to);
    }

    public function delete($id=false)
    {
        // Model
        $bahanModel = new BahanModel;
        $data = $bahanModel->find($id);

        if(empty($data)) {
            $this->notFound($this->$module_name);
        }else{
            $bahanModel->delete($id);
            $this->delSuccess($this->$module_name);
        }

        return redirect()->to($this->redirect_to);
    }
}
