<?php namespace App\Controllers;

use App\Models\StokModel;
use App\Models\BahanModel;
use App\Models\DetailStokModel;

class Stok extends BaseController
{
    protected $redirect_to = '/stok';
    protected $module_name = 'Data Inventory';
    protected $jenis_produk = ['makanan', 'minuman'];
    protected $pg = 10;

    public function index()
    {
        // Data
        $data['title'] = "Manajemen Inventory Restoran";

        // Model
        $stokModel = new StokModel;
        $data['dataObj'] = $stokModel->findAll();
        // $data['pager'] = $stokModel->pager;

        return $this->renderTemplate('stok/home',$data);
    }

    public function create()
    {
        // Data
        $data['title'] = "Form - Manajemen Inventory Restoran";

        $bahanModel = new BahanModel;
        $data['bahans'] = $bahanModel->findAll();
        $data['jenis_produk'] = $this->jenis_produk;

        return $this->renderTemplate('stok/form',$data);
    }

    public function edit($id=false)
    {
        // Data
        $data['title'] = "Form - Manajemen Inventory Restoran";

        // Model
        $stokModel = new StokModel;
        $bahanModel = new BahanModel;
        $detailStokModel = new DetailStokModel;
        $data['dataSet'] = $stokModel->find($id);
        if(empty($data['dataSet'])) {
            $this->notFound($this->$module_name);
            return redirect()->to($this->redirect_to);
        }else{
            $data['details'] = $detailStokModel->where('stok_id',$id)->findAll();
        }

        $data['bahans'] = $bahanModel->findAll();
        $data['jenis_produk'] = $this->jenis_produk;

        return $this->renderTemplate('stok/form',$data);
    }

    public function store()
    {
        // Ngambil Object
        $post = $this->request->getPost();
        $stokModel = new StokModel;

        // Validasi Form
        $validation = $this->validation;
        if($post['id']) {
            $validation->setRules($stokModel->formRules('edit'));
        }else{
            $validation->setRules($stokModel->formRules());
        }

        // Form Validation Gagal
        if($validation->withRequest($this->request)->run() == FALSE){
            return $this->failValidation($validation->getErrors());
        }

        // Proses Data
        $data = [
            'tanggal' => $post['tanggal'],
            'total_harga' => $post['total_harga'],
            'user_id' => $this->session->get('loggedUser')['user_id'],
        ];
        if($post['id']) {
            $stokModel->update($post['id'],$data);
            $stok_id = $post['id'];
        }else{
            $stokModel->insert($data);
            $stok_id = $stokModel->getInsertID();
        }

        // Resep
        $detailStokModel = new DetailStokModel;
        $detailStokModel->where('stok_id',$stok_id)->delete();
        $resep = [];
        foreach($post['bahan'] as $k=>$v) {
            $resep[] = [
                'bahan_id' => $v,
                'stok_id' => $stok_id,
                'jumlah' => $post['jumlah'][$k],
                'tanggal_kadaluarsa' => $post['tanggal_kadaluarsa'][$k],
                'satuan' => $post['satuan'][$k],
                'harga_satuan' => $post['harga_satuan'][$k],
                'harga_total' => $post['harga_total'][$k],
            ];
        }
        $detailStokModel->insertBatch($resep);
        
        $this->saveSuccess($this->$module_name);
        return redirect()->to($this->redirect_to);
    }

    public function delete($id=false)
    {
        // Model
        $stokModel = new StokModel;
        $detailStokModel = new DetailStokModel;
        $data = $stokModel->find($id);

        if(empty($data)) {
            $this->notFound($this->$module_name);
        }else{
            $detailStokModel->where('stok_id',$id)->delete();
            $stokModel->delete($id);
            $this->delSuccess($this->$module_name);
        }

        return redirect()->to($this->redirect_to);
    }
}
