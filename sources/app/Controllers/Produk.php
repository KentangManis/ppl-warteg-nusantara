<?php namespace App\Controllers;

use App\Models\ProdukModel;
use App\Models\BahanModel;
use App\Models\ResepModel;

class Produk extends BaseController
{
    protected $redirect_to = '/produk';
    protected $module_name = 'Data Menu Produk';
    protected $jenis_produk = ['makanan', 'minuman'];
    protected $pg = 10;

    public function index()
    {
        // Data
        $data['title'] = "Manajemen Menu Produk Restoran";

        // Model
        $produkModel = new ProdukModel;
        $data['dataObj'] = $produkModel->findAll();
        // $data['pager'] = $produkModel->pager;

        return $this->renderTemplate('produk/home',$data);
    }

    public function create()
    {
        // Data
        $data['title'] = "Form - Manajemen Menu Produk Restoran";

        $bahanModel = new BahanModel;
        $data['bahans'] = $bahanModel->findAll();
        $data['jenis_produk'] = $this->jenis_produk;

        return $this->renderTemplate('produk/form',$data);
    }

    public function edit($id=false)
    {
        // Data
        $data['title'] = "Form - Manajemen Menu Produk Restoran";

        // Model
        $produkModel = new ProdukModel;
        $bahanModel = new BahanModel;
        $resepModel = new ResepModel;
        $data['dataSet'] = $produkModel->find($id);
        if(empty($data['dataSet'])) {
            $this->notFound($this->$module_name);
            return redirect()->to($this->redirect_to);
        }else{
            $data['reseps'] = $resepModel->where('produk_id',$id)->findAll();
        }

        $data['bahans'] = $bahanModel->findAll();
        $data['jenis_produk'] = $this->jenis_produk;

        return $this->renderTemplate('produk/form',$data);
    }

    public function store()
    {
        // Ngambil Object
        $post = $this->request->getPost();
        $produkModel = new ProdukModel;

        // Validasi Form
        $validation = $this->validation;
        if($post['id']) {
            $validation->setRules($produkModel->formRules('edit'));
        }else{
            $validation->setRules($produkModel->formRules());
        }

        // Form Validation Gagal
        if($validation->withRequest($this->request)->run() == FALSE){
            return $this->failValidation($validation->getErrors());
        }

        // Proses Data
        $data = [
            'nama' => $post['nama'],
            'stok' => $post['stok'],
            'jenis' => $post['jenis'],
            'harga' => $post['harga'],
        ];
        if($post['id']) {
            $produkModel->update($post['id'],$data);
            $produk_id = $post['id'];
        }else{
            $produkModel->insert($data);
            $produk_id = $produkModel->getInsertID();
        }

        // Resep
        $resepModel = new ResepModel;
        $resepModel->where('produk_id',$produk_id)->delete();
        $resep = [];
        foreach($post['bahan'] as $k=>$v) {
            $resep[] = [
                'bahan_id' => $v,
                'produk_id' => $produk_id,
                'jumlah' => $post['jumlah'][$k],
                'satuan' => $post['satuan'][$k],
            ];
        }
        $resepModel->insertBatch($resep);
        
        $this->saveSuccess($this->$module_name);
        return redirect()->to($this->redirect_to);
    }

    public function delete($id=false)
    {
        // Model
        $produkModel = new ProdukModel;
        $resepModel = new ResepModel;

        $data = $produkModel->find($id);

        if(empty($data)) {
            $this->notFound($this->$module_name);
        }else{
            $resepModel->where('produk_id',$id)->delete();
            $produkModel->delete($id);
            $this->delSuccess($this->$module_name);
        }

        return redirect()->to($this->redirect_to);
    }
}
