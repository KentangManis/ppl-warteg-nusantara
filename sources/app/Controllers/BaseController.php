<?php
namespace App\Controllers;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use CodeIgniter\Controller;

class BaseController extends Controller
{

    /**
     * An array of helpers to be loaded automatically upon
     * class instantiation. These helpers will be available
     * to all other controllers that extend BaseController.
     *
     * @var array
     */
    protected $helpers = ['form','session'];

    /**
     * Constructor.
     */
    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        //--------------------------------------------------------------------
        // Preload any models, libraries, etc, here.
        //--------------------------------------------------------------------
        // E.g.:
        $this->db = \Config\Database::connect();
        $this->session = \Config\Services::session();
        $this->validation =  \Config\Services::validation();
    }

    public function renderTemplate($template,$data)
    {
        $data['loggedUser'] = $this->session->get('loggedUser');
        $data['template_js'] = $template.'_js';

        // View
        echo view('layouts/header', $data);
        echo view('layouts/sidebar', $data);
        echo view($template, $data);
        echo view('layouts/footer', $data);
    }

    public function debug($data)
    {
        echo "<pre>";
        print_r($data);
        exit;
    }

    public function failValidation($errors)
    {
        return redirect()->back()
        ->withInput()
        ->with('errors', $errors);
    }

    public function saveSuccess($msg=null)
    {
        $this->session->setFlashdata('success', (($msg) ?? 'Data').' BERHASIL disimpan/update');
    }

    public function saveFail($msg=null)
    {
        $this->session->setFlashdata('failed', (($msg) ?? 'Data').' GAGAL disimpan/update');
    }

    public function delSuccess($msg=null)
    {
        $this->session->setFlashdata('success', (($msg) ?? 'Data').' BERHASIL dihapus');
    }

    public function delFail($msg=null)
    {
        $this->session->setFlashdata('failed', (($msg) ?? 'Data').' GAGAL dihapus');
    }

    public function notFound($msg=null)
    {
        $this->session->setFlashdata('failed', (($msg) ?? 'Data').' tidak ditemukan!');
    }

}
