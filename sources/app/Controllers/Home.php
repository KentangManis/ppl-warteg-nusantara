<?php namespace App\Controllers;

use App\Models\ProdukModel;
use App\Models\BahanModel;
use App\Models\OrderModel;
use App\Models\UserModel;

class Home extends BaseController
{
    public function index()
    {
        $produkModel = new ProdukModel;
        $bahanModel = new BahanModel;
        $orderModel = new OrderModel;
        $userModel = new UserModel;

        $data['produks'] = $produkModel->countAll();
        $data['bahans'] = $bahanModel->countAll();
        $data['users'] = $userModel->countAll();
        $data['orders'] = $orderModel->countAll();

        return $this->renderTemplate('dashboard',$data);
    }

    //--------------------------------------------------------------------

}
