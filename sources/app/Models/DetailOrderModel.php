<?php namespace App\Models;

use CodeIgniter\Model;

class DetailOrderModel extends Model
{
    protected $table = 'detail_orders';
    protected $allowedFields = ['order_id','produk_id','jumlah','harga_satuan','harga_total'];
    protected $useTimestamps = false;
}