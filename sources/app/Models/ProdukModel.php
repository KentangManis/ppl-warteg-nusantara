<?php namespace App\Models;

use CodeIgniter\Model;

class ProdukModel extends Model
{
    protected $table = 'produks';
    protected $allowedFields = ['nama','jenis','stok','harga'];
    protected $useTimestamps = false;

    public function formRules($tipe=null)
    {
        return [
            'nama' => [
                'label' => 'Nama Produk',
                'rules' => 'required'
            ],
            'jenis' => [
                'label' => 'Jenis',
                'rules' => 'required'
            ],
            'stok' => [
                'label' => 'Stok',
                'rules' => 'required'
            ],
            'harga' => [
                'label' => 'Harga',
                'rules' => 'required'
            ],
        ];
    }
}