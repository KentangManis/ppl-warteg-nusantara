<?php namespace App\Models;

use CodeIgniter\Model;

class BahanModel extends Model
{
    protected $table = 'bahans';
    protected $allowedFields = ['nama','stok','harga'];
    protected $useTimestamps = false;

    public function formRules($tipe=null)
    {
        return [
            'nama' => [
                'label' => 'Nama Bahan',
                'rules' => 'required'
            ],
            'stok' => [
                'label' => 'Stok',
                'rules' => 'required'
            ],
            'harga' => [
                'label' => 'Harga',
                'rules' => 'required'
            ],
        ];
    }
}