<?php namespace App\Models;

use CodeIgniter\Model;

class ResepModel extends Model
{
    protected $table = 'reseps';
    protected $allowedFields = ['bahan_id','produk_id','jumlah','satuan'];
    protected $useTimestamps = false;
}