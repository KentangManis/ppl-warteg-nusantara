<?php namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table = 'users';

    public function authRules()
    {
        return [
            'username' => [
                'label' => 'Username',
                'rules' => 'required'
            ],
            'password' => [
                'label' => 'Password',
                'rules' => 'required'
            ],
        ];
    }

    public function formRules()
    {
        return [
            'nama' => [
                'label' => 'Username',
                'rules' => 'required'
            ],
            'username' => [
                'label' => 'Username',
                'rules' => 'required'
            ],
            'password' => [
                'label' => 'Password',
                'rules' => 'required'
            ],
            'jabatan' => [
                'label' => 'Password',
                'rules' => 'required'
            ],
        ];
    }
}