<?php namespace App\Models;

use CodeIgniter\Model;

class OrderModel extends Model
{
    protected $table = 'orders';
    protected $allowedFields = ['user_id','tanggal','total_harga','jumlah','nominal_bayar','nominal_kembalian'];
    protected $useTimestamps = false;

    public function formRules($tipe=null)
    {
        return [
            'tanggal' => [
                'label' => 'Tanggal',
                'rules' => 'required|date'
            ],
            'total_harga' => [
                'label' => 'Total Harga',
                'rules' => 'required'
            ],
            'jumlah' => [
                'label' => 'Jumlah',
                'rules' => 'required'
            ],
            'nominal_bayar' => [
                'label' => 'Nominal Bayar',
                'rules' => 'required'
            ],
            'nominal_kembalian' => [
                'label' => 'Nominal Kembalian',
                'rules' => 'required'
            ],
        ];
    }
}