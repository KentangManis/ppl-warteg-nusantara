<?php namespace App\Models;

use CodeIgniter\Model;

class StokModel extends Model
{
    protected $table = 'stoks';
    protected $allowedFields = ['user_id','tanggal','total_harga'];
    protected $useTimestamps = false;

    public function formRules($tipe=null)
    {
        return [
            'tanggal' => [
                'label' => 'Tanggal',
                'rules' => 'required|date'
            ],
            'total_harga' => [
                'label' => 'Harga',
                'rules' => 'required'
            ],
        ];
    }
}