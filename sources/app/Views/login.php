<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Warteg Nusantara | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo csrf_meta() ?>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url('admin/plugins/fontawesome-free/css/all.min.css') ?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url('admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css') ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('admin/dist/css/adminlte.min.css') ?>">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="<?php echo site_url('/') ?>"><b>Warteg</b> Nusantara</a>
      </div>
      <!-- /.login-logo -->
      <div class="card">
        <div class="card-body login-card-body">
          <p class="login-box-msg">Sign in to start your session</p>
          <?php $errors = session()->getFlashdata('errors')?>
          <?php if(!empty($errors) && !is_array($errors)){ ?>
            <div class="alert alert-danger" role="alert">
              Whoops! Error : <?php echo $errors?>
            </div>
          <?php } ?>
          <form action="<?php echo site_url('auth/login') ?>" method="post">
            <?php echo csrf_field() ?>
            <div class="form-group mb-3">
              <?php if(!empty($errors) && (is_array($errors) && array_key_exists('username', $errors))):?>
              <div class="input-group is-invalid">
              <?php else:?>
              <div class="input-group">
              <?php endif;?>
                <input type="text" name="username" class="form-control" placeholder="Username">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-envelope"></span>
                  </div>
                </div>
              </div>
              <?php if(!empty($errors) && (is_array($errors) && array_key_exists('username', $errors))):?>
              <span class="error invalid-feedback">
                  <strong><?php echo $errors['username']?></strong>
              </span>
              <?php endif;?>
            </div>
            <div class="form-group mb-3">
              <?php if(!empty($errors) && (is_array($errors) && array_key_exists('username', $errors))):?>
              <div class="input-group is-invalid">
              <?php else:?>
              <div class="input-group">
              <?php endif;?>
                <input type="password" name="password" class="form-control" placeholder="Password">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                  </div>
                </div>
              </div>
              <?php if(!empty($errors) && (is_array($errors) && array_key_exists('password', $errors))):?>
              <span class="invalid-feedback">
                  <strong><?php echo $errors['password']?></strong>
              </span>
              <?php endif;?>
            </div>
            <div class="row">
              <div class="col-8">
                <div class="icheck-primary">
                  <input type="checkbox" id="remember">
                  <label for="remember">
                    Remember Me
                  </label>
                </div>
              </div>
              <!-- /.col -->
              <div class="col-4">
                <button type="submit" class="btn btn-primary btn-block">Sign In</button>
              </div>
              <!-- /.col -->
            </div>
          </form>
        </div>
        <!-- /.login-card-body -->
      </div>
    </div>
    <!-- /.login-box -->

  <!-- jQuery -->
  <script src="<?php echo base_url('admin/plugins/jquery/jquery.min.js') ?>"></script>
  <!-- Bootstrap 4 -->
  <script src="<?php echo base_url('admin/plugins/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
  <!-- AdminLTE App -->
  <script src="<?php echo base_url('admin/dist/js/adminlte.min.js') ?>"></script>

  </body>
</html>
