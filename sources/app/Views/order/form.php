  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Point of Sales</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php base_url('home')?>">Home</a></li>
              <li class="breadcrumb-item active">Point of Sales</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <form id="orderForm" action="<?php echo base_url('order/store') ?>" method="POST">
      <input type="hidden" class="form-control" name="id" value="<?php echo old('id') ?? (isset($dataSet) ? $dataSet['id'] : '' )?>"/>
      <?php echo csrf_field() ?>

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"><?php echo isset($dataSet) ? 'Edit' : 'Create' ?> Stok Inventory</h3>

          <div class="card-tools">
            <a class="btn btn-default btn-sm" title="Inventory List" href="<?php echo base_url('order') ?>"><i class="fa fas-table"></i> List</a>
            <a class="btn btn-primary btn-sm" title="Register Inventory" href="<?php echo base_url('order/create') ?>"><i class="fa fas-plus"></i> Create</a>
          </div>
        </div>
        <div class="card-body">
            <?php if(!empty(session('errors')) && !is_array(session('errors'))){ ?>
              <div class="alert alert-danger" role="alert">
                Whoops! Error : <?php echo session('errors')?>
              </div>
            <?php } ?>
            <div class="row">
                <div class="col-md-3 col-sm-4">
                    <div class="form-group">
                        <label for="tanggal">Tanggal Order <span class="text-red danger">*</span></label>
                        <input type="date" class="form-control <?php if(session('errors.tanggal')):?> is-invalid <?php endif;?>" id="tanggal" name="tanggal" value="<?php echo old('tanggal') ?? (isset($dataSet) ? $dataSet['tanggal'] : date("Y-m-d") )?>" required/>
                        <?php if(session('errors.tanggal')):?>
                            <span class="error invalid-feedback">
                                <strong><?php echo session('errors.tanggal')?></strong>
                            </span>
                        <?php endif;?>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4">
                    <div class="form-group">
                        <label for="total_harga">TOTAL HARGA </label>
                        <input type="text" class="form-control decimalonly <?php if(session('errors.total_harga')):?> is-invalid <?php endif;?>" id="total_harga" name="total_harga" value="<?php echo old('total_harga', isset($dataSet) ? $dataSet['total_harga'] : '' )?>" readonly/>
                         <?php if(session('errors.total_harga')):?>
                            <span class="error invalid-feedback">
                                <strong><?php echo session('errors.total_harga')?></strong>
                            </span>
                        <?php endif;?>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4">
                    <div class="form-group">
                        <label for="nominal_bayar">Dibayar </label>
                        <input type="text" class="form-control <?php if(session('errors.nominal_bayar')):?> is-invalid <?php endif;?>" id="nominal_bayar" name="nominal_bayar" value="<?php echo old('nominal_bayar', isset($dataSet) ? $dataSet['nominal_bayar'] : '' )?>"/>
                         <?php if(session('errors.nominal_bayar')):?>
                            <span class="error invalid-feedback">
                                <strong><?php echo session('errors.nominal_bayar')?></strong>
                            </span>
                        <?php endif;?>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4">
                    <div class="form-group">
                        <label for="nominal_kembalian">Kembalian </label>
                        <input type="text" class="form-control decimalonly <?php if(session('errors.nominal_kembalian')):?> is-invalid <?php endif;?>" id="nominal_kembalian" name="nominal_kembalian" value="<?php echo old('nominal_kembalian', isset($dataSet) ? $dataSet['nominal_kembalian'] : '' )?>" readonly/>
                         <?php if(session('errors.nominal_kembalian')):?>
                            <span class="error invalid-feedback">
                                <strong><?php echo session('errors.nominal_kembalian')?></strong>
                            </span>
                        <?php endif;?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <hr/>
                    </div>
                    <table id="produkList" class="table" width="100%">
                      <thead>
                        <tr>
                          <th>
                            <button type="button" class="btn btn-primary btn-sm" onclick="produkDuplicate()"><i class="fa fa-plus"></i></button>
                          </th>
                          <th width="50%">Pilih produk</th>
                          <th>Jumlah</th>
                          <th>Harga Satuan (Rp)</th>
                          <th>Harga Total (Rp)</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr style="display: none;" id="produkDuplicate">
                          <td>
                            <button type="button" class="btn btn-danger btn-sm" onclick="$(this).closest('tr').remove();hitungTotal();"><i class="fa fa-times"></i></button>
                          </td>
                          <td>
                            <select class="form-control pick-produk" disabled name="produk[]" required>
                              <option value="">-- Pilih produk --</option>
                                <?php foreach($produks as $row):?>
                                  <option value="<?php echo $row['id']?>" data-harga="<?php echo $row['harga']?>"><?php echo $row['nama']?></option>
                                <?php endforeach;?>
                            </select>
                          </td>
                          <td>
                            <input type="text" class="form-control decimalonly" disabled name="jumlah[]" value="" required/>
                          </td>
                          <td>
                            <input type="text" class="form-control decimalonly" disabled name="harga_satuan[]" value="" required/>
                          </td>
                          <td>
                            <input type="text" class="form-control decimalonly" disabled name="harga_total[]" value="" readonly />
                          </td>
                        </tr>
                        <?php if(isset($dataSet) && !empty($details)):?>
                        <?php foreach($details as $detail):?>
                        <tr>
                          <td>
                            <button type="button" class="btn btn-danger btn-sm" onclick="$(this).closest('tr').remove();hitungTotal();"><i class="fa fa-times"></i></button>
                          </td>
                          <td>
                            <select class="form-control pick-produk" name="produk[]" required>
                              <option value="">-- Pilih Produk --</option>
                                <?php foreach($produks as $row):?>
                                  <?php if($row['id'] == $detail['produk_id']):?>
                                  <option value="<?php echo $row['id']?>" data-harga="<?php echo $row['harga']?>" selected><?php echo $row['nama']?></option>
                                  <?php else:?>
                                  <option value="<?php echo $row['id']?>" data-harga="<?php echo $row['harga']?>"><?php echo $row['nama']?></option>
                                  <?php endif;?>
                                <?php endforeach;?>
                            </select>
                          </td>
                          <td>
                            <input type="text" class="form-control decimalonly" name="jumlah[]" value="<?php echo $detail['jumlah'] ?>" required/>
                          </td>
                          <td>
                            <input type="text" class="form-control decimalonly" name="harga_satuan[]" value="<?php echo $detail['harga_satuan'] ?>" required/>
                          </td>
                          <td>
                            <input type="text" class="form-control decimalonly" name="harga_total[]" value="<?php echo $detail['harga_total'] ?>" readonly/>
                          </td>
                        </tr>
                        <?php endforeach;?>
                        <?php endif;?>
                      </tbody>
                    </table>
                </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-info" id="orderSubmit">Process</button>
            <a class="btn btn-outline-danger" href="<?php base_url('order') ?>">Cancel</a>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

      </form>
    </section>
    <!-- /.content -->
  </div>