<script type="text/javascript">
    function produkDuplicate() {
        var row = $('#produkDuplicate').clone();
        row.show().find('input, select').removeAttr('disabled');

        $('#produkList tbody').append(row);
    }

    $('#produkList tbody').on('change','.pick-produk',function() {
        var harga = $(this).find(':selected').data('harga');
        var parent_tr = $(this).closest('tr');
        parent_tr.find('input[name^=harga_satuan]').val(harga).change();
    });
    $('#produkList tbody').on('change keydown keypress keyup','input[name^=jumlah], input[name^=harga_satuan]',function() {
        var parent_tr = $(this).closest('tr');
        var harga_satuan = parent_tr.find('input[name^=harga_satuan]').val();
        var jumlah = parent_tr.find('input[name^=jumlah]').val();
        parent_tr.find('input[name^=harga_total]').val(harga_satuan*jumlah).change();
    });

    $('#produkList tbody').on('change','input[name^=harga_total]',function() {
        var sum = 0;
        $('#produkList tbody').find('input[name^=harga_total]').each(function(){
            if($(this).val() != '') {
                sum += parseFloat($(this).val());
            }
        });
        $('#total_harga').val(sum).change();
    });

    $('#nominal_bayar, #total_harga').on('change keydown keypress keyup',function() {
        var harga = $('#total_harga').val();
        var bayar = $('#nominal_bayar').val();
        $('#nominal_kembalian').val(bayar-harga).change();

        if((bayar-harga) < 0){
            $('#orderSubmit').attr('disabled');
        }else{
            $('#orderSubmit').removeAttr('disabled');
        }
    });

    function hitungTotal() {
        var sum = 0;
        $('#produkList tbody').find('input[name^=harga_total]').each(function(){
            if($(this).val() != '') {
                sum += parseFloat($(this).val());
            }
        });
        $('#total_harga').val(sum).change();
    }
</script>