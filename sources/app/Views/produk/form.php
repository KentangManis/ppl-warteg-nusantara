  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Manajemen Produk</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php base_url('home')?>">Home</a></li>
              <li class="breadcrumb-item active">Manajemen Produk</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <form action="<?php echo base_url('produk/store') ?>" method="POST">
      <input type="hidden" class="form-control" name="id" value="<?php echo old('id') ?? (isset($dataSet) ? $dataSet['id'] : '' )?>"/>
      <?php echo csrf_field() ?>

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"><?php echo isset($dataSet) ? 'Edit' : 'Create' ?> Produk</h3>

          <div class="card-tools">
            <a class="btn btn-default btn-sm" title="Produk List" href="<?php echo base_url('produk') ?>"><i class="fa fas-table"></i> List</a>
            <a class="btn btn-primary btn-sm" title="Register Produk" href="<?php echo base_url('produk/create') ?>"><i class="fa fas-plus"></i> Create</a>
          </div>
        </div>
        <div class="card-body">
            <?php if(!empty(session('errors')) && !is_array(session('errors'))){ ?>
              <div class="alert alert-danger" role="alert">
                Whoops! Error : <?php echo session('errors')?>
              </div>
            <?php } ?>
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                        <label for="nama">Nama Produk <span class="text-red danger">*</span></label>
                        <input type="text" class="form-control <?php if(session('errors.nama')):?> is-invalid <?php endif;?>" id="nama" name="nama" value="<?php echo old('nama') ?? (isset($dataSet) ? $dataSet['nama'] : '' )?>"/>
                        <?php if(session('errors.nama')):?>
                            <span class="error invalid-feedback">
                                <strong><?php echo session('errors.nama')?></strong>
                            </span>
                        <?php endif;?>
                    </div>
                    <div class="form-group">
                        <label for="price">Harga </label>
                        <input type="text" class="form-control decimalonly <?php if(session('errors.harga')):?> is-invalid <?php endif;?>" id="harga" name="harga" value="<?php echo old('harga', isset($dataSet) ? $dataSet['harga'] : '' )?>"/>
                         <?php if(session('errors.harga')):?>
                            <span class="error invalid-feedback">
                                <strong><?php echo session('errors.harga')?></strong>
                            </span>
                        <?php endif;?>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                        <label for="jenis">Kategori </label>
                        <select class="form-control select2 <?php if(session('errors.jenis')):?> is-invalid <?php endif;?>" id="jenis" name="jenis">
                            <?php foreach($jenis_produk as $v):?>
                              <?php if($v == old('jenis')):?>
                                <option value="<?php echo $v?>" selected><?php echo ucfirst($v)?></option>
                              <?php elseif(isset($dataSet) && $dataSet['jenis'] == $v):?>
                                <option value="<?php echo $v?>" selected><?php echo ucfirst($v)?></option>
                              <?php else:?>
                                <option value="<?php echo $v?>"><?php echo ucfirst($v)?></option>
                              <?php endif;?>
                            <?php endforeach;?>
                        </select>
                        <?php if(session('errors.jenis')):?>
                            <span class="error invalid-feedback">
                                <strong><?php echo session('errors.jenis')?></strong>
                            </span>
                        <?php endif;?>
                    </div>
                    <div class="form-group">
                        <label for="stok">Stok </label>
                        <input type="text" class="form-control decimalonly <?php if(session('errors.stok')):?> is-invalid <?php endif;?>" id="stok" name="stok" value="<?php echo old('stok', isset($dataSet) ? $dataSet['stok'] : '' )?>"/>
                         <?php if(session('errors.stok')):?>
                            <span class="error invalid-feedback">
                                <strong><?php echo session('errors.stok')?></strong>
                            </span>
                        <?php endif;?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="nama">Resep (per 100 porsi)<span class="text-red danger">*</span></label>
                        <hr/>
                    </div>
                    <table id="resepList" class="table" width="100%">
                      <thead>
                        <tr>
                          <th>
                            <button type="button" class="btn btn-primary btn-sm" onclick="resepDuplicate()"><i class="fa fa-plus"></i></button>
                          </th>
                          <th width="50%">Pilih Bahan</th>
                          <th>Jumlah</th>
                          <th>Satuan</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr style="display: none;" id="resepDuplicate">
                          <td>
                            <button type="button" class="btn btn-danger btn-sm" onclick="$(this).closest('tr').remove();"><i class="fa fa-times"></i></button>
                          </td>
                          <td>
                            <select class="form-control" disabled name="bahan[]">
                              <option value="">-- Pilih Bahan --</option>
                                <?php foreach($bahans as $row):?>
                                  <option value="<?php echo $row['id']?>"><?php echo $row['nama']?></option>
                                <?php endforeach;?>
                            </select>
                          </td>
                          <td>
                            <input type="text" class="form-control" disabled name="jumlah[]" value=""/>
                          </td>
                          <td>
                            <input type="text" class="form-control" disabled name="satuan[]" value=""/>
                          </td>
                        </tr>
                        <?php if(isset($dataSet) && !empty($reseps)):?>
                        <?php foreach($reseps as $resep):?>
                        <tr>
                          <td>
                            <button type="button" class="btn btn-danger btn-sm" onclick="$(this).closest('tr').remove();"><i class="fa fa-times"></i></button>
                          </td>
                          <td>
                            <select class="form-control" name="bahan[]">
                              <option value="">-- Pilih Bahan --</option>
                                <?php foreach($bahans as $row):?>
                                  <?php if($row['id'] == $resep['bahan_id']):?>
                                  <option value="<?php echo $row['id']?>" selected><?php echo $row['nama']?></option>
                                  <?php else:?>
                                  <option value="<?php echo $row['id']?>"><?php echo $row['nama']?></option>
                                  <?php endif;?>
                                <?php endforeach;?>
                            </select>
                          </td>
                          <td>
                            <input type="text" class="form-control" name="jumlah[]" value="<?php echo $resep['jumlah'] ?>"/>
                          </td>
                          <td>
                            <input type="text" class="form-control" name="satuan[]" value="<?php echo $resep['satuan'] ?>"/>
                          </td>
                        </tr>
                        <?php endforeach;?>
                        <?php endif;?>
                      </tbody>
                    </table>
                </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-info">Process</button>
            <a class="btn btn-outline-danger" href="<?php base_url('produk') ?>">Cancel</a>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

      </form>
    </section>
    <!-- /.content -->
  </div>

<script type="text/javascript">
  function resepDuplicate() {
    var row = $('#resepDuplicate').clone();
    row.show().find('input, select').removeAttr('disabled');

    $('#resepList tbody').append(row);
  }
</script>