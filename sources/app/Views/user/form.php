  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Manajemen User</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php base_url('home')?>">Home</a></li>
              <li class="breadcrumb-item active">Manajemen User</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <form action="<?php echo base_url('user/store') ?>" method="POST">
      <input type="hidden" class="form-control" name="id" value="<?php echo old('id') ?? (isset($dataSet) ? $dataSet['id'] : '' )?>"/>
      <?php echo csrf_field() ?>

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"><?php echo isset($dataSet) ? 'Edit' : 'Create' ?> User</h3>

          <div class="card-tools">
            <a class="btn btn-default btn-sm" title="User List" href="<?php echo base_url('user') ?>"><i class="fa fas-table"></i> List</a>
            <a class="btn btn-primary btn-sm" title="Register User" href="<?php echo base_url('user/create') ?>"><i class="fa fas-plus"></i> Create</a>
          </div>
        </div>
        <div class="card-body">
            <?php if(!empty(session('errors')) && !is_array(session('errors'))){ ?>
              <div class="alert alert-danger" role="alert">
                Whoops! Error : <?php echo session('errors')?>
              </div>
            <?php } ?>
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                        <label for="nama">Nama User <span class="text-red danger">*</span></label>
                        <input type="text" class="form-control <?php if(session('errors.nama')):?> is-invalid <?php endif;?>" id="nama" name="nama" value="<?php echo old('nama') ?? (isset($dataSet) ? $dataSet['nama'] : '' )?>"/>
                        <?php if(session('errors.nama')):?>
                            <span class="error invalid-feedback">
                                <strong><?php echo session('errors.nama')?></strong>
                            </span>
                        <?php endif;?>
                    </div>
                    <div class="form-group">
                        <label for="price">Harga </label>
                        <input type="text" class="form-control decimalonly <?php if(session('errors.harga')):?> is-invalid <?php endif;?>" id="harga" name="harga" value="<?php echo old('harga', isset($dataSet) ? $dataSet['harga'] : '' )?>"/>
                         <?php if(session('errors.harga')):?>
                            <span class="error invalid-feedback">
                                <strong><?php echo session('errors.harga')?></strong>
                            </span>
                        <?php endif;?>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                        <label for="stok">Stok </label>
                        <input type="text" class="form-control decimalonly <?php if(session('errors.stok')):?> is-invalid <?php endif;?>" id="stok" name="stok" value="<?php echo old('stok', isset($dataSet) ? $dataSet['stok'] : '' )?>"/>
                         <?php if(session('errors.stok')):?>
                            <span class="error invalid-feedback">
                                <strong><?php echo session('errors.stok')?></strong>
                            </span>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-info">Process</button>
            <a class="btn btn-outline-danger" href="<?php base_url('user') ?>">Cancel</a>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

      </form>
    </section>
    <!-- /.content -->
  </div>
