  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Manajemen User</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('home')?>">Home</a></li>
              <li class="breadcrumb-item active">Manajemen User</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Daftar User</h3>

          <div class="card-tools">
            <a class="btn btn-primary btn-sm" title="User List" href="<?php echo base_url('user') ?>"><i class="fa fas-table"></i> List</a>
            <a class="btn btn-default btn-sm" title="Register User" href="<?php echo base_url('user/create') ?>"><i class="fa fas-plus"></i> Create</a>
          </div>
        </div>
        <div class="card-body">
          <table id="userTable" class="table table-bordered table-hover" width="100%">
            <thead>
              <tr>
                <th width="5%">No.</th>
                <th width="12%">Action</th>
                <th>Nama User</th>
                <th>Username</th>
                <th>Jabatan</th>
              </tr>
            </thead>
            <tbody>
              <?php if(!empty($dataObj)):?>
              <?php $i=1;foreach($dataObj as $row):?>
              <tr>
                <td><?php echo $i++;?>.</td>
                <td>
                  <!-- <a class="btn btn-warning btn-sm" title="Edit" href="<?php echo base_url('user/edit/'.$row['id']) ?>">Edit</a> -->
                  <a class="btn btn-warning btn-sm" title="Edit" href="#">Edit</a>
                  <!-- <a class="btn btn-danger btn-sm" title="Delete" href="<?php echo base_url('user/delete/'.$row['id']) ?>" onclick="return deleteRow();">Del</a> -->
                  <a class="btn btn-danger btn-sm" title="Delete" href="#" onclick="return deleteRow();">Del</a>
                </td>
                <td><?php echo $row['nama']?></td>
                <td><?php echo $row['username']?></td>
                <td><?php echo ucfirst($row['jabatan'])?></td>
              </tr>
              <?php endforeach;?>
              <?php else:?>
              <tr>
                <td colspan="15" class="text-center">Data Kosong</td>
              </tr>
              <?php endif;?>
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
