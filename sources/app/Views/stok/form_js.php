<script type="text/javascript">
    function bahanDuplicate() {
        var row = $('#bahanDuplicate').clone();
        row.show().find('input, select').removeAttr('disabled');

        $('#bahanList tbody').append(row);
    }

    $('#bahanList tbody').on('change','.pick-bahan',function() {
        var harga = $(this).find(':selected').data('harga');
        var parent_tr = $(this).closest('tr');
        parent_tr.find('input[name^=harga_satuan]').val(harga).change();
    });

    $('#bahanList tbody').on('change keypress','input[name^=jumlah], input[name^=harga_satuan]',function() {
        var parent_tr = $(this).closest('tr');
        var harga_satuan = parent_tr.find('input[name^=harga_satuan]').val();
        var jumlah = parent_tr.find('input[name^=jumlah]').val();
        parent_tr.find('input[name^=harga_total]').val(harga_satuan*jumlah).change();
    });

    $('#bahanList tbody').on('change','input[name^=harga_total]',function() {
        var sum = 0;
        $('#bahanList tbody').find('input[name^=harga_total]').each(function(){
            if($(this).val() != '') {
                sum += parseFloat($(this).val());
            }
        });
        $('#total_harga').val(sum).change();
    });

    function hitungTotal() {
        var sum = 0;
        $('#bahanList tbody').find('input[name^=harga_total]').each(function(){
            if($(this).val() != '') {
                sum += parseFloat($(this).val());
            }
        });
        $('#total_harga').val(sum).change();
    }
</script>