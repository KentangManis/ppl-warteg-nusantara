<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo isset($title) ? $title : 'Dashboard'?> - Warteg Nusantara</title>
    <?php echo csrf_meta() ?>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url('admin/plugins/fontawesome-free/css/all.min.css')?>">
    <!-- Select 2 -->
    <link rel="stylesheet" href="<?php echo base_url('admin/plugins/select2/css/select2.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')?>">
    <!-- Sweetalert 2 -->
    <link rel="stylesheet" href="<?php echo base_url('admin/plugins/sweetalert2/sweetalert2.min.css')?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('admin/dist/css/adminlte.min.css')?>">

    <!-- jQuery -->
    <script src="<?php echo base_url('admin/plugins/jquery/jquery.min.js')?>"></script>

  </head>
  <body class="hold-transition sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
          <a href="<?php echo base_url('home') ?>" class="nav-link">Home</a>
        </li>
      </ul>

      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
        <!-- <li class="nav-item">
          <a class="nav-link btn btn-default ml-2" href="{{base_url('user.profile')}}" role="button">Profile</a>
        </li> -->
        <li class="nav-item">
          <a class="nav-link btn btn-default ml-2" href="#" role="button" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
          <form id="logout-form" action="<?php echo base_url('auth/logout') ?>" method="POST" style="display: none;">
            <?php echo csrf_field() ?>
          </form>
        </li>
      </ul>
    </nav>
    <!-- /.navbar -->