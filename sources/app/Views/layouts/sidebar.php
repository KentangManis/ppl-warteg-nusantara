    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="<?php echo base_url('home') ?>" class="brand-link">
        <img src="<?php echo base_url('admin/dist/img/logo.png')?>" alt="Warteg Nusantara" class="brand-image" style="opacity: .8">
        <span class="brand-text font-weight-light">Warteg Nusantara</span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <img src="<?php echo base_url('admin/dist/img/avatar.png')?>" class="img-circle elevation-2" alt="User Image">
          </div>
          <div class="info">
            <a href="#" class="d-block"><?php echo isset($loggedUser) && $loggedUser['nama'] != '' ? $loggedUser['nama'] : 'Whoisme?' ?></a>
          </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                 with font-awesome or any other icon font library -->
            <li class="nav-item">
              <a href="<?php echo base_url('user') ?>" class="nav-link">
                <i class="nav-icon fas fa-users"></i>
                <p>Manajemen User</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('produk') ?>" class="nav-link">
                <i class="nav-icon fas fa-list"></i>
                <p>Manajemen Produk</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('bahan') ?>" class="nav-link">
                <i class="nav-icon fa fa-shopping-cart"></i>
                <p>Manajemen Bahan</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('order') ?>" class="nav-link">
                <i class="nav-icon fas fa-dollar-sign"></i>
                <p>Transaksi</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('stok') ?>" class="nav-link">
                <i class="nav-icon fas fa-box"></i>
                <p>Inventory</p>
              </a>
            </li>
          </ul>
        </nav>
        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>
