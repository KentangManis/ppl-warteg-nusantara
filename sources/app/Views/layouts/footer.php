  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2020 <a href="<?php base_url('/dashboard')?>">Warteg Nusantara</a>.</strong> All rights reserved.
  </footer>
</div>

<!-- Bootstrap 4 -->
<script src="<?php echo base_url('admin/plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<!-- Select 2 -->
<script src="<?php echo base_url('admin/plugins/select2/js/select2.min.js')?>"></script>
<!-- Sweetalert 2 -->
<script src="<?php echo base_url('admin/plugins/sweetalert2/sweetalert2.min.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('admin/dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('admin/dist/js/demo.js')?>"></script>

<!-- ./wrapper -->
<script type="text/javascript">
    $('.decimalonly').bind('keypress keyup paste', function(){
        this.value = this.value.match(/^[0-9]*\.?[0-9]*$/);
    });
    $('.select2').select2();

    function deleteRow() {
        return true;

        return Swal.fire({
            icon : "warning",
            text : "Yakin ingin menghapus data ini?",
            showCancelButton: true,
        });
    }

    $(document).ready(function() {
    <?php if(!empty(session('failed'))){ ?>
        Swal.fire({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            icon: 'error',
            title: 'Oops...',
            text: '<?php echo session('failed')?>',
        });
    <?php } ?>

    <?php if(!empty(session('success'))){ ?>
        Swal.fire({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            icon: 'success',
            title: 'Yeay!',
            text: '<?php echo session('success')?>',
        });
    <?php } ?>
    });
</script>

<!-- ./template_js -->
<?php echo (is_file(APPPATH.'Views/'.$template_js.'.php')) ? view($template_js) : '';?>

</body>
</html>