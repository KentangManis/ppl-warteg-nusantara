-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 08, 2021 at 02:02 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kuliah_ppl_warteg`
--

-- --------------------------------------------------------

--
-- Table structure for table `bahans`
--

DROP TABLE IF EXISTS `bahans`;
CREATE TABLE IF NOT EXISTS `bahans` (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `stok` int(5) NOT NULL DEFAULT 0,
  `harga` decimal(18,2) NOT NULL DEFAULT 0.00,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bahans`
--

INSERT INTO `bahans` (`id`, `nama`, `stok`, `harga`) VALUES
(1, 'Beras', 1, '11000.00'),
(2, 'Bawang', 1, '15000.00'),
(3, 'Garam', 1, '12000.00');

-- --------------------------------------------------------

--
-- Table structure for table `detail_orders`
--

DROP TABLE IF EXISTS `detail_orders`;
CREATE TABLE IF NOT EXISTS `detail_orders` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(5) UNSIGNED NOT NULL,
  `produk_id` int(5) UNSIGNED NOT NULL,
  `jumlah` int(5) UNSIGNED NOT NULL,
  `harga_satuan` decimal(18,2) NOT NULL,
  `harga_total` decimal(18,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `detail_orders_produk_id_foreign` (`produk_id`),
  KEY `detail_orders_order_id_foreign` (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `detail_stoks`
--

DROP TABLE IF EXISTS `detail_stoks`;
CREATE TABLE IF NOT EXISTS `detail_stoks` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `stok_id` int(5) UNSIGNED NOT NULL,
  `bahan_id` int(5) UNSIGNED NOT NULL,
  `tanggal_kadaluarsa` date NOT NULL,
  `jumlah` int(5) UNSIGNED NOT NULL,
  `satuan` varchar(25) NOT NULL,
  `harga_satuan` decimal(18,2) NOT NULL,
  `harga_total` decimal(18,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `detail_stoks_bahan_id_foreign` (`bahan_id`),
  KEY `detail_stoks_stok_id_foreign` (`stok_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `version` varchar(255) NOT NULL,
  `class` text NOT NULL,
  `group` varchar(255) NOT NULL,
  `namespace` varchar(255) NOT NULL,
  `time` int(11) NOT NULL,
  `batch` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `version`, `class`, `group`, `namespace`, `time`, `batch`) VALUES
(73, '2020-12-28-165356', 'App\\Database\\Migrations\\CreateTabelUsers', 'default', 'App', 1610105594, 1),
(74, '2020-12-28-165403', 'App\\Database\\Migrations\\CreateTabelBahans', 'default', 'App', 1610105594, 1),
(75, '2020-12-28-165410', 'App\\Database\\Migrations\\CreateTabelProduks', 'default', 'App', 1610105594, 1),
(76, '2020-12-28-165417', 'App\\Database\\Migrations\\CreateTabelReseps', 'default', 'App', 1610105595, 1),
(77, '2020-12-28-175423', 'App\\Database\\Migrations\\CreateTabelStoks', 'default', 'App', 1610105595, 1),
(78, '2020-12-28-185424', 'App\\Database\\Migrations\\CreateTabelDetailStoks', 'default', 'App', 1610105595, 1),
(79, '2020-12-28-195430', 'App\\Database\\Migrations\\CreateTabelOrders', 'default', 'App', 1610105595, 1),
(80, '2020-12-28-205438', 'App\\Database\\Migrations\\CreateTabelDetailOrders', 'default', 'App', 1610105595, 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(5) UNSIGNED NOT NULL,
  `tanggal` date NOT NULL,
  `total_harga` decimal(18,2) NOT NULL,
  `jumlah` int(5) UNSIGNED NOT NULL,
  `nominal_bayar` decimal(18,2) NOT NULL,
  `nominal_kembalian` decimal(18,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_user_id_foreign` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `produks`
--

DROP TABLE IF EXISTS `produks`;
CREATE TABLE IF NOT EXISTS `produks` (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `jenis` enum('makanan','minuman') NOT NULL,
  `stok` int(5) NOT NULL DEFAULT 0,
  `harga` decimal(18,2) NOT NULL DEFAULT 0.00,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `produks`
--

INSERT INTO `produks` (`id`, `nama`, `jenis`, `stok`, `harga`) VALUES
(1, 'Telur Balado', 'makanan', 1, '5000.00'),
(2, 'Ayam Bakar Madu', 'makanan', 1, '15000.00'),
(3, 'Sayur Lodeh', 'makanan', 1, '5000.00'),
(4, 'Nasi Putih', 'makanan', 1, '2000.00');

-- --------------------------------------------------------

--
-- Table structure for table `reseps`
--

DROP TABLE IF EXISTS `reseps`;
CREATE TABLE IF NOT EXISTS `reseps` (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bahan_id` int(5) UNSIGNED DEFAULT NULL,
  `produk_id` int(5) UNSIGNED DEFAULT NULL,
  `jumlah` int(5) NOT NULL DEFAULT 0,
  `satuan` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `reseps_bahan_id_foreign` (`bahan_id`),
  KEY `reseps_produk_id_foreign` (`produk_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `stoks`
--

DROP TABLE IF EXISTS `stoks`;
CREATE TABLE IF NOT EXISTS `stoks` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(5) UNSIGNED NOT NULL,
  `tanggal` date NOT NULL,
  `total_harga` decimal(18,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `stoks_user_id_foreign` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `jabatan` enum('kasir','koki','owner','admin','guest') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nama`, `username`, `password`, `jabatan`) VALUES
(1, 'Admin Warteg', 'admin', '$2y$10$6Y0.LYkSIlJMoT3nojMCC.bHRejPW0pfO8BQE7q/z6gyZFVPM8HpO', 'admin'),
(2, 'Kasir Warteg', 'kasir', '$2y$10$Ainj9R4qsp5KNR9HZudmn../jRpjkjWmba/NtlMPXIc1jP1iT2b9u', 'kasir'),
(3, 'Koki Warteg', 'koki', '$2y$10$NeMlMeb0G2w5/yuNQF1UHuKbH4Plkc1uyUv/nlYDNZoxv9VDCC2xe', 'koki'),
(4, 'Owner Warteg', 'owner', '$2y$10$WuVL5tv93JzgYH/C/XkaJO99NnIZTYUGcnswmLxkTkpp92VpLwXf6', 'owner');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_orders`
--
ALTER TABLE `detail_orders`
  ADD CONSTRAINT `detail_orders_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `detail_orders_produk_id_foreign` FOREIGN KEY (`produk_id`) REFERENCES `produks` (`id`);

--
-- Constraints for table `detail_stoks`
--
ALTER TABLE `detail_stoks`
  ADD CONSTRAINT `detail_stoks_bahan_id_foreign` FOREIGN KEY (`bahan_id`) REFERENCES `bahans` (`id`),
  ADD CONSTRAINT `detail_stoks_stok_id_foreign` FOREIGN KEY (`stok_id`) REFERENCES `stoks` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `reseps`
--
ALTER TABLE `reseps`
  ADD CONSTRAINT `reseps_bahan_id_foreign` FOREIGN KEY (`bahan_id`) REFERENCES `bahans` (`id`),
  ADD CONSTRAINT `reseps_produk_id_foreign` FOREIGN KEY (`produk_id`) REFERENCES `produks` (`id`);

--
-- Constraints for table `stoks`
--
ALTER TABLE `stoks`
  ADD CONSTRAINT `stoks_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
